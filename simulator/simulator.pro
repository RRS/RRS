TEMPLATE = subdirs

CONFIG += ordered

SUBDIRS += ./sound-manager
SUBDIRS += ./physics
SUBDIRS += ./device
SUBDIRS += ./solver
SUBDIRS += ./rkf5
SUBDIRS += ./rk4
SUBDIRS += ./euler2
SUBDIRS += ./euler
SUBDIRS += ./vehicle
SUBDIRS += ./coupling
SUBDIRS += ./profile
SUBDIRS += ./train
SUBDIRS += ./sim-client
SUBDIRS += ./signaling
SUBDIRS += ./model
SUBDIRS += ./default-coupling
SUBDIRS += ./ef-coupling
SUBDIRS += ./simulator

SUBDIRS += ./modbus

SUBDIRS += ./krm395
SUBDIRS += ./krm130
SUBDIRS += ./vr242
SUBDIRS += ./vr483
SUBDIRS += ./ar265
SUBDIRS += ./kvt224
SUBDIRS += ./kvt254
SUBDIRS += ./epk150
SUBDIRS += ./evr305
SUBDIRS += ./carbrakes-mech
SUBDIRS += ./hose369a
SUBDIRS += ./joint-pneumo-hose
SUBDIRS += ./joint-pneumo-hose-epb
