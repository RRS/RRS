#ifndef VENICLE_SIGNALS_H
#define VENICLE_SIGNALS_H

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
enum
{
    MAX_DISCRETE_SIGNALS = 100,
    MAX_ANALOG_SIGNALS = 500,
    INPUTS_NUMBER = 100,
    OUTPUTS_NUMBER = 100
};

#endif // VENICLE_SIGNALS_H
