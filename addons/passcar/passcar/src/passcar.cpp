#include    "passcar.h"
#include    "filesystem.h"
#include    "passcar-signals.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
PassCar::PassCar() : Vehicle ()
  , brakepipe(nullptr)
  , bp_leak(0.0)
  , air_dist(nullptr)
  , air_dist_module("vr242")
  , air_dist_config("vr242")
  , electro_air_dist(nullptr)
  , electro_air_dist_module("")
  , electro_air_dist_config("")
  , supply_reservoir(nullptr)
  , sr_volume(0.078)
  , sr_leak(0.0)
  , anglecock_bp_fwd(nullptr)
  , anglecock_bp_bwd(nullptr)
  , anglecock_bp_config("pneumo-anglecock-BP")
  , hose_bp_fwd(nullptr)
  , hose_bp_bwd(nullptr)
  , hose_bp_module("hose369a")
  , hose_bp_config("pneumo-hose-BP369a-passcar")
  , brake_mech(nullptr)
  , brake_mech_config("carbrakes-mech-composite")
  , ip(2.96)
{

}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
PassCar::~PassCar()
{

}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void PassCar::initialization()
{
    FileSystem &fs = FileSystem::getInstance();
    QString modules_dir(fs.getModulesDir().c_str());

    initBrakesEquipment(modules_dir);

    initEPB(modules_dir);

    initSounds();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void PassCar::step(double t, double dt)
{
    stepBrakesEquipment(t, dt);

    stepEPB(t, dt);

    stepSignalsOutput();

    stepDebugMsg(t, dt);

    soundStep();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void PassCar::keyProcess()
{
    if (!hose_bp_bwd->isConnected())
    {
        if (getKeyState(KEY_BackSpace))
        {
            if (isShift())
                anglecock_bp_bwd->open();
            else
                anglecock_bp_bwd->close();
        }
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void PassCar::loadConfig(QString cfg_path)
{
    CfgReader cfg;

    if (cfg.load(cfg_path))
    {
        QString secName = "Vehicle";

        cfg.getDouble(secName, "BrakepipeLeak", bp_leak);

        cfg.getString(secName, "AirDistModule", air_dist_module);
        cfg.getString(secName, "AirDistConfig", air_dist_config);

        cfg.getString(secName, "ElectroAirDistModule", electro_air_dist_module);
        cfg.getString(secName, "ElectroAirDistConfig", electro_air_dist_config);

        cfg.getDouble(secName, "SupplyReservoirVolume", sr_volume);
        cfg.getDouble(secName, "SupplyReservoirLeak", sr_leak);

        cfg.getString(secName, "BrakepipeAnglecockConfig", anglecock_bp_config);

        cfg.getString(secName, "BrakepipeHoseModule", hose_bp_module);
        cfg.getString(secName, "BrakepipeHoseConfig", hose_bp_config);

        cfg.getString(secName, "BrakeMechConfig", brake_mech_config);

        cfg.getDouble(secName, "GenReductorCoeff", ip);
    }
}

GET_VEHICLE(PassCar)
